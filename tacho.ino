//Include LCD library
#include <LiquidCrystal.h>
#include <math.h>


// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

//Buttons
const int buttonAPin = A0;
const int buttonBPin = A1;
const int buttonCPin = A2;
const int buttonDPin = A3;
const int hallEffectsTrig = A4;


//Button states
int buttonAState = 0;
int buttonBState = 0;
int buttonCState = 0;
int hallEffectsTrig = 0;
// HMM HOW WE DOING THE TERMISTOR!


//View
int currentView = 0; //0 = Overview, 1 = RPM, 2 = Temp
int currentMenu = false;
int menuOpen = false;
int currentSetting = 0;
int newSetting = currentSetting;

//Speed settings
const int isTimingTachometer = false;

const int settings[5][3] = {
  { 0, false, 0000 }, //ID,Coolant, Target RPM
  { 1, true, 1253 },
  { 2, true, 1057 },
  { 3, false, 452 },
  { 4, true, 1756 },
};

const String settingsNames[5] = {
  {"None       "},
  {"Steel 1    "},
  {"Acrylic 1  "},
  {"Briar 1    "},
  {"Delrin     "},
};

//Starting Values
int rpm = 998;              //Current Spindle RPM
int temp = 95;              //Motor Temp
int cool = false;           //Use collant
int targetRPM = false;      //The defined RPM Tarhet
float differncePC = 0;      //Used for the target calculations
String rpmChange = "--";    //Used to show the change in speed



void setup() {

  Serial.begin(9600);
  Serial.println("--- Start Serial Monitor SEND_RCVE ---");
  Serial.println(" Type in Box above, . ");
  Serial.println("(Decimal)(Hex)(Character)");
  Serial.println();
  Serial.println(settings[currentSetting][3]);
  Serial.println(settingsNames[currentSetting]);



  //Set buttons as inputs
  pinMode(buttonAPin, INPUT);
  pinMode(buttonBPin, INPUT);
  pinMode(buttonCPin, INPUT);
  pinMode(buttonDPin, INPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(13, OUTPUT);

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  //Loading screen
  lcd.print("Tachometer");
  lcd.setCursor(0, 1);
  lcd.print("Loading...");
  delay(1250);
}

void loop() {

  //Match new setting with current
  //newSetting = currentSetting;

  //Button presses (Define)
  buttonAState = digitalRead(buttonAPin);
  buttonBState = digitalRead(buttonBPin);
  buttonCState = digitalRead(buttonCPin);
  buttonDState = digitalRead(buttonDPin);
  hallEffectsTrigState = digitalRead(hallEffectsTrig);

  //Start timer baced on the LOW signal detecction
  if (hallEffectsTrigState == LOW) {
    //Start the timer if we are not already
    if (isTimingTachometer == true) {
      //HMM SOMETHING HAS GONE WRONG
    } else {
      isTimingTachometer = true;
      //Start the time

    }
  }

  //Button presses (Watch)

  //Button A (Toggle Modes)
  if (buttonAState == HIGH) {
    if(currentView == 2){
      currentView = 0;
      //Close the menu if left open and full cycle
      menuOpen = false;
    }else{
      currentView = currentView + 1;
    }
    delay(200);
  }

  //Button B (Menu/Set)
  if (buttonBState == HIGH) {
    //Check if in menu or not, do not allow if on overview.
    if(currentView != 0){

      if(menuOpen){ //Menu Open
        menuOpen = false;
        //If the RPM setting is open, update the setting
        if (currentView == 1){
          currentSetting = newSetting;
          //Update Details
          updateRPMSettings();
          //Return to overview
          currentView = 0;
        }
      }else{ //Not Open
        menuOpen = true;
      }
      delay(200);
    }


  }

  //Button C (Up)
  if (buttonCState == HIGH) {
    if(!menuOpen){
      rpm = rpm - 50;
    }else{
      //If menu is open, do action based on menuopen
      if(currentView == 1){
        if(newSetting == 0){
           newSetting = 4;
        }else{
          newSetting = newSetting - 1;
        }
      }
    }
    delay(200);
  }

  //Button D (Down)
  if (buttonDState == HIGH) {
    if(!menuOpen){
      rpm = rpm + 50;
    }

    //If menu is open, do action based on menuopen
    if(currentView == 1){

      if(newSetting == 4){
         newSetting = 0;
      }else{
        newSetting = newSetting + 1;
      }
    }
    delay(200);
  }

  //Check if we have a desired speed and display via leds if we are above or below
  if(targetRPM || targetRPM > 0){

    //Calculate the different between current and target
    int difference = targetRPM - rpm;

    differncePC = (float)difference / (float)targetRPM;
    Serial.print(differncePC);
    delay(100);

    digitalWrite(6, LOW);  //Too Fast
    digitalWrite(7, LOW);  //Fast
    digitalWrite(8, LOW);  //Perfect
    digitalWrite(9, LOW);  //Slow
    digitalWrite(10, LOW); //Too Slow

    if(differncePC <= -0.15){
      digitalWrite(6, HIGH);
    }else if(differncePC <= -0.05 && differncePC > -0.15){
      digitalWrite(7, HIGH);
    }else if(differncePC <= 0.05 && differncePC > -0.05){
      digitalWrite(8, HIGH);
    }else if(differncePC <= 0.15 && differncePC > 0.05){
      digitalWrite(9, HIGH);
    }else if(differncePC > 0.15){
      digitalWrite(10, HIGH);
    }

  }


  //The View
  renderView();

}



//Print int as string
void printIntAsStr(int i, int places){
  if(i < 10 && places > 1){
      lcd.print("0");
  }
  if(i < 100 && places > 2){
      lcd.print("0");
  }
  if(i < 1000 && places > 3){
      lcd.print("0");
  }
  lcd.print(i);
}

//Display Current RPM
void displayCurrentRPM(){
  //If the current RPM is 0, show idle and trigger the blue led
  if(!rpm || rpm == 0){
    lcd.print("IDLE");
  }else{
    printIntAsStr(rpm, 4);
  }
}

//Display Current RPM Target
void displayCurrentTargetRPM(){
  //If the current RPM is 0, show idle and trigger the blue led
  if(!targetRPM || targetRPM == 0){
    lcd.print("....");
  }else{
    printIntAsStr(targetRPM, 4);
  }
}

//Display current coolant
void displayCoolantStatus(){
  if (cool){
    lcd.print("Cool ON   ");
    digitalWrite(13, HIGH);
  }else{
    lcd.print("NO Cool   ");
    digitalWrite(13, LOW);
  }
}

//Views
void displayOverView(){
  lcd.setCursor(0, 0);
  lcd.print("RPM:");
  lcd.setCursor(4, 0);
  displayCurrentRPM();
  lcd.print("    ");
  lcd.setCursor(12, 0);
  displayCurrentTargetRPM();
  lcd.setCursor(0, 1);
  displayCoolantStatus();
  lcd.print("   ");
  lcd.setCursor(12, 1);
  printIntAsStr(temp, 3);
  lcd.write(0xDF);

}

void displayRPMView(){
  lcd.setCursor(0, 0);
  lcd.print("RPM:");
  lcd.setCursor(4, 0);
  displayCurrentRPM();
  lcd.setCursor(8, 0);
  lcd.print("        ");
  lcd.setCursor(0, 1);
  lcd.print(settingsNames[currentSetting]);
  lcd.setCursor(12, 1);
  displayCurrentTargetRPM();
}

void displayCoolant(){
  lcd.setCursor(0, 0);
  displayCoolantStatus();
  lcd.setCursor(8, 0);
  lcd.print("        ");
  lcd.setCursor(0, 1);
  lcd.print(settingsNames[currentSetting]);

}

void displayRPMSettings(){
  lcd.setCursor(0, 0);
  lcd.print("RPM:Settings");
  lcd.setCursor(0, 1);
  lcd.print(settingsNames[newSetting]);
  lcd.setCursor(12, 1);
  if(currentSetting == newSetting){
    lcd.print("    ");
  }else{
    lcd.print("set?");
  }

}

//Render the view based on the settings
void renderView(){
  //Check if we are in a menu
  if(menuOpen){
    //Show correct Menu based on current view
    switch (currentView) {
      case 1:
        displayRPMSettings();
      break;

      case 2:
        displayCoolant();
      break;

      default:
        displayOverView();
      break;
    }
  }else{
    //Show correct Menu based on current view
    switch (currentView) {
      case 1:
        displayRPMView();
      break;

      case 2:
        displayCoolant();
      break;

      default:
        displayOverView();
      break;
    }
  }
}

//Update the RPM Setting on change
void updateRPMSettings(){
  targetRPM = settings[currentSetting][2];
  cool = settings[currentSetting][1];
}
